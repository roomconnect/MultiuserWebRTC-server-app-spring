package com.example.demo.persistence.model;

public enum Role {
    USER, ADMIN
}
